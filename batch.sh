#!/bin/bash -l
#SBATCH --constraint=gpu
#SBATCH --nodes=1
#SBATCH --gpus-per-node=4
#SBATCH --tasks-per-node=32
#SBATCH --cpus-per-task=2
#SBATCH --time=10
#SBATCH --qos=regular
#SBATCH --job-name=gpu_specter
#SBATCH --output=slurm-%j.out
#SBATCH --account=m1759_g

echo "Start time: $(date --iso-8601=seconds)"
pwd; hostname;

# OpenMP settings
export OMP_NUM_THREADS=1

# Specify input exposure
indir=/global/cfs/cdirs/m1759/dmargala/everest
night=20210505
expid=00087380

# Output directory
# Note: this script will remove all fits files from here after the benchmark
outdir=$SCRATCH
joboutdir=${outdir}/temp-${SLURM_JOB_ID}
mkdir -p ${joboutdir}

export MPICH_GPU_SUPPORT_ENABLED=1

logfile=slurm-$SLURM_JOB_ID.out

# Run the benchmark
srun -n 22 -c 2 --cpu-bind=cores \
    desi_mps_wrapper \
    desi_benchmark_extract $indir $joboutdir $(date +%s) --night $night --expid $expid --gpu --async-io --pixpad-frac 0.8 --wavepad-frac 0.2 --nwavestep 50
# Clean up
rm -rf ${joboutdir}

echo "End time: $(date --iso-8601=seconds)"

