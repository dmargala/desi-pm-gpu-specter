# desi-pm-gpu-specter

Run the DESI 30-frame extraction benchmark on perlmutter. The benchmark script `desi_benchmark_extract` and GPU extraction code live here: https://github.com/desihub/gpu_specter

## Initial setup

```
ssh perlmutter

cd $SCRATCH
git clone git@gitlab.com:dmargala/desi-pm-gpu-specter.git
cd desi-pm-gpu-specter

module load PrgEnv-gnu
module load cpe-cuda
module load cuda/11.3.0
module load python

conda env create -f env-conda.yml --name pm-gpu-specter

source activate pm-gpu-specter

cat env-pip-extra.txt | xargs -l pip install

MPICC="cc -shared" pip install --force --no-cache-dir --no-binary=mpi4py mpi4py

pip install cupy-cuda113 -f https://github.com/cupy/cupy/releases/v9.4.0
```

## Usage after setup

```
ssh perlmutter

cd $SCRATCH/desi-pm-gpu-specter

module load PrgEnv-gnu
module load cpe-cuda
module load cuda/11.3.0
module load python

source activate pm-gpu-specter

#note, you may need to adjust the account used in batch.sh, it is currently m1759_g
sbatch batch.sh
```



